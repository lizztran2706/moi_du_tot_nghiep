<?php

namespace App\Http\Controllers;

use App\Models\viewTrangChu;
use Illuminate\Http\Request;

class ViewTrangChuController extends Controller
{
    public function index()
    {
        return view('admin.share.home_page');
    }
}
