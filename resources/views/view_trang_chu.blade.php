<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tốt Nghiệp 2023</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <p><b>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio, aperiam quidem ipsa velit consequatur voluptatum dolores, vitae recusandae aliquam rem, laboriosam asperiores delectus quis quam est veritatis numquam omnis neque!
                        Iste quaerat hic quasi delectus! Sapiente, suscipit possimus facere repellat doloribus facilis voluptatibus eligendi impedit, ullam voluptates, voluptatum commodi. Eos vel perferendis ex obcaecati iure et iusto, eum voluptatum aspernatur!
                        Reiciendis, voluptate. Commodi voluptatem a rem, itaque illo nostrum autem ipsam dignissimos id incidunt dolor eaque necessitatibus minima quasi! Debitis culpa architecto illo odio libero illum fuga. Tempora, hic totam.</b></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>
