<?php

use App\Http\Controllers\ViewTrangChuController;
use App\Models\viewTrangChu;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/tot-nghiep-2023'], function() {
    Route::group(['prefix' => '/chuyen-muc'], function() {
        Route::get('/index',[ChuyenMucController::class, 'index']);
        Route::post('create',[ChuyenMucController::class, 'store']);
        Route::post('data',[ChuyenMucController::class, 'data']);
        Route::get('/edit/{id}',[ChuyenMucController::class, 'edit']);
        Route::get('doi-trang-thai/{id}', [ChuyenMucController::class, 'doiTrangThai']);
        Route::post('/update',[ChuyenMucController::class, 'update']);
        Route::get('/delete/{id}',[ChuyenMucController::class, 'destroy']);
    });

    Route::group(['prefix' => '/moi-ban'], function() {
        Route::get('/index',[ViewTrangChuController::class, 'index']);

    });

});
